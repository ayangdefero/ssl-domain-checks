var gulp = require('gulp');
var gutil = require('gulp-util');
var connect = require('gulp-connect');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var htmlSources = ['**/*.html'];

gulp.task('sass', function() {
  gulp.src('assets/scss/app.scss')
  .pipe(sass({style: 'expanded'}))
    .on('error', gutil.log)
  .pipe(gulp.dest('assets'))
  .pipe(connect.reload())
});

gulp.task('watch', function() {
  gulp.watch(['assets/scss/app.scss','assets/scss/partials/*.scss'], ['sass']);
  gulp.watch(htmlSources, ['html']);
});

gulp.task('html', function() {
  gulp.src(htmlSources)
  .pipe(connect.reload())
});

gulp.task('connect', function() {
  connect.server({
    root: '.',
    livereload: true
  })
});

gulp.task('default', ['html',  'sass', 'connect', 'watch']);
