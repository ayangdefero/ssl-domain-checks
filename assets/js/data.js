// // Initialize the Amazon Cognito credentials provider
AWS.config.region = 'us-west-2'; // Region
AWS.config.accessKeyId="AKIAIWGBGYA2BN6VB4RA";
AWS.config.secretAccessKey="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";
// AWS.config.credentials = new AWS.CognitoIdentityCredentials({
//   IdentityPoolId: 'us-west-2:928107b4-89ea-46cb-af31-0e26a6cfcd77',
// });
// Prepare to call Lambda function
var lambda = new AWS.Lambda({region: 'us-west-2', apiVersion: '2015-03-31'});
var pullParams = {
  FunctionName : 'domain-ssl-check-dev-ssl',
  InvocationType : 'RequestResponse',
  LogType : 'None'
};

var whoisLambda = new AWS.Lambda({region: 'us-west-2', apiVersion: '2015-03-31'});
var whoisPullParams = {
  FunctionName : 'domain-ssl-check-dev-whois',
  InvocationType : 'RequestResponse',
  LogType : 'None'
};

var pingdomLambda = new AWS.Lambda({region: 'us-west-2', apiVersion: '2015-03-31'});
var pingdomPullParams = {
  FunctionName : 'domain-ssl-check-dev-pingdomcheck',
  InvocationType : 'RequestResponse',
  LogType : 'None'
};

function addPingdomTableRow(data) {
  var dbClient = new AWS.DynamoDB.DocumentClient();
  var params = {
    TableName: "PingdomInformation",
    Key :{
      "id": data.id
    },
    UpdateExpression: "set dateCreated = :dateCreated, siteName = :siteName, hostname = :hostname, siteType = :siteType, lastErrorTime = :lastErrorTime, lastTestTime = :lastTestTime, siteStatus = :siteStatus",
    ExpressionAttributeValues: {
      ":dateCreated": new Date(data.created*1000).toLocaleString(),
      ":siteName": data.name,
      ":hostname": data.hostname,
      ":siteType": data.type,
      ":lastErrorTime": new Date(data.lasterrortime*1000).toLocaleString(),
      ":lastTestTime": new Date(data.lasttesttime*1000).toLocaleString(),
      ":siteStatus": data.status
    }
  }

  dbClient.update(params, function(err, data) {
    if(err) {
      console.error("Unable to add site",". Error JSON:", JSON.stringify(err, null, 2));
    } else {
      console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
    }
  });
}

function displayTable(data) {
  data = JSON.parse(data);
  if(data) {
    $(".ssl-table tbody").empty();
    for (var i = 0, len = data.length; i < len; i++) {
      addTableRow(data[i]);
    }
  }

  $('.get-table-btn').click();
}
function displayWhoIsTable(data) {
  data = JSON.parse(data);
  console.log(data);
  if(data) {
    $(".domain-table tbody").empty();
    for (var i = 0, len = data.length; i < len; i++) {
      addWhoIsTableRow(data[i]);
    }
  }

  $('.get-whois-table-btn').click();
}

function displayPingdomTable(data) {
  if(data) {
    $(".pingdom-table tbody").empty();
    for (var i = 0, len = data.length; i < len; i++) {
      addPingdomTableRow(data[i]);
    }
  }

  $('.get-pingdom-table-btn').click();
}

function addWhoIsTableRow(data){
  var name = data.name;
  var url = "www." + data.url;
  var registrarExpiration = new Date(data.registrarExpiration);
  registrarExpiration = registrarExpiration.toLocaleString();
  var registrarServer = data.registrarServer;
  var registrarURL = data.registrarURL;
  var registrarName = data.registrarName;
  var domainName = data.domainName;
  var siteStatus = data.status;

  var dbClient = new AWS.DynamoDB.DocumentClient();
  var params = {
    TableName: "WhoIsInformation",
    Key :{
      "url": url,
      "name": name
    },
    UpdateExpression: "set domainName = :domainName, registrarName = :registrarName, registrarServer = :registrarServer, registrarURL = :registrarURL, registrarExpiration = :registrarExpiration, siteStatus = :siteStatus, dateUpdated = :dateUpdated",
    ExpressionAttributeValues: {
      ":domainName": domainName,
      ":registrarName": registrarName,
      ":registrarURL": registrarURL,
      ":registrarServer": registrarServer,
      ":registrarExpiration": registrarExpiration,
      ":siteStatus" : siteStatus,
      ":dateUpdated": new Date().toLocaleString()
    }
  }

  dbClient.update(params, function(err, data) {
    if(err) {
      console.error("Unable to add site", url, ". Error JSON:", JSON.stringify(err, null, 2));
    } else {
      console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
    }
  });
  var mxRecords = "";
  if(data.mxRecord) {
    for(var i = 0; i < data.mxRecord.length; i++) {
      mxRecords += "Exchange: " + data.mxRecord[i].exchange + " Priority: " + data.mxRecord[i].priority + "</br>";
    }
  }
  var dnsparams = {
    TableName: "DNSInformation",
    Key :{
      "url": url,
      "name": name
    },
    UpdateExpression: "set aRecord = :aRecord, aaaaRecord = :aaaaRecord, mxRecord = :mxRecord, txtRecord = :txtRecord, srvRecord = :srvRecord, ptrRecord = :ptrRecord, nsRecord = :nsRecord, cnameRecord = :cnameRecord, dateUpdated = :dateUpdated",
    ExpressionAttributeValues:{
      ":aRecord": data.aRecord ? data.aRecord.join("<br/>") : "No A Record Info",
      ":aaaaRecord": data.aaaaRecord ? data.aaaaRecord.join("<br/>") : "No AAAA Record Info",
      ":mxRecord": data.mxRecord ? mxRecords : "No MX Record Info",
      ":txtRecord": data.txtRecord ? data.txtRecord.join("<br/>") : "No TXT Record Info",
      ":srvRecord": data.srvRecord ? data.srvRecord.join("<br/>") : "No SRV Record Info",
      ":ptrRecord" : data.ptrRecord ? data.ptrRecord.join("<br/>") : "No PTR Record Info",
      ":nsRecord" : data.nsRecord ? data.nsRecord.join("<br/>") : "No NS Record Info",
      ":cnameRecord" : data.cnameRecord ? data.cnameRecord.join("<br/>") : "No CNAME Record Info",
      ":dateUpdated": new Date().toLocaleString()
    }
  }

  dbClient.update(dnsparams, function(err, data) {
    if(err) {
      console.error("Unable to add site", url, ". Error JSON:", JSON.stringify(err, null, 2));
    } else {
      console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
    }
  });
}
function addTableRow(data){
  var name = data.name;
  var url = data.url;
  var expiration = new Date(data.expiration);
  expiration = expiration.toLocaleString();
  var status = data.status;
  //
  // var $row = $("<tr/>");
  // if(status == "red") {
  //   $row.append("<td>" + name + "</td><td>" + url + "</td><td>" + expiration+ "</td><td class=" + status + "><i class='fa fa-exclamation-square'></i></td><td></td>");
  // } else {
  //   $row.append("<td>" + name + "</td><td>" + url + "</td><td>" + expiration+ "</td><td class=" + status + "><i class='fa fa-badge-check'></i></td><td></td>");
  // }
  //
  // $(".ssl-table tbody").append($row);

  //add to db

  var dbClient = new AWS.DynamoDB.DocumentClient();
  var params = {
    TableName: "SSLInformation",
    Key :{
      "url": url,
      "name": name
    },
    UpdateExpression: "set expiration = :expiration, siteStatus = :siteStatus, dateUpdated =:dateUpdated ",
    ExpressionAttributeValues: {
      ":expiration": expiration,
      ":siteStatus": status,
      ":dateUpdated": new Date().toLocaleString()
    }
  }

  dbClient.update(params, function(err, data) {
    if(err) {
      console.error("Unable to add site", url, ". Error JSON:", JSON.stringify(err, null, 2));
    } else {
      console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
    }
  });
}

$(function() {
  //ssl buttons
  $('.update-ssl').click(function() {
    lambda.invoke(pullParams, function(err, data) {
      if (err) {
        prompt(err);
      } else {
        pullResults = JSON.parse(data.Payload);
        displayTable(pullResults);
      }
    });
  });
  $('.create-table-btn').click(function() {

    var dynamodb = new AWS.DynamoDB();

    var params = {
      TableName : "SSLInformation",
      KeySchema: [
        { AttributeName: "url", KeyType: "HASH"},  //Partition key
        { AttributeName: "name", KeyType: "RANGE" }  //Sort key
      ],
      AttributeDefinitions: [
        { AttributeName: "url", AttributeType: "S" },
        { AttributeName: "name", AttributeType: "S" }
      ],
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    };

    dynamodb.createTable(params, function(err, data) {
      if (err) {
        console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
      } else {
        console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
      }
    });
  });

  $('.create-whois-table-btn').click(function() {

    var dynamodb = new AWS.DynamoDB();

    var params = {
      TableName : "WhoIsInformation",
      KeySchema: [
        { AttributeName: "url", KeyType: "HASH"},  //Partition key
        { AttributeName: "name", KeyType: "RANGE" }  //Sort key
      ],
      AttributeDefinitions: [
        { AttributeName: "url", AttributeType: "S" },
        { AttributeName: "name", AttributeType: "S" }
      ],
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    };

    dynamodb.createTable(params, function(err, data) {
      if (err) {
        console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
      } else {
        console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
      }
    });
  });

  $('.create-dns-table-btn').click(function() {

    var dynamodb = new AWS.DynamoDB();

    var params = {
      TableName : "DNSInformation",
      KeySchema: [
        { AttributeName: "url", KeyType: "HASH"},  //Partition key
        { AttributeName: "name", KeyType: "RANGE" }  //Sort key
      ],
      AttributeDefinitions: [
        { AttributeName: "url", AttributeType: "S" },
        { AttributeName: "name", AttributeType: "S" }
      ],
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    };

    dynamodb.createTable(params, function(err, data) {
      if (err) {
        console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
      } else {
        console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
      }
    });
  });

  $('.create-pingdom-table-btn').click(function() {

    var dynamodb = new AWS.DynamoDB();

    var params = {
      TableName : "PingdomInformation",
      KeySchema: [
        { AttributeName: "id", KeyType: "HASH"},  //Partition key
      ],
      AttributeDefinitions: [
        { AttributeName: "id", AttributeType: "N" }
      ],
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
      }
    };

    dynamodb.createTable(params, function(err, data) {
      if (err) {
        console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
      } else {
        console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
      }
    });
  });

  $('.get-whois-table-btn').click(function() {

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
      TableName: "WhoIsInformation"
    };
    docClient.scan(params, onWhoIsScan);
  }).click();

  $('.get-dns-table-btn').click(function() {

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
      TableName: "DNSInformation"
    };
    docClient.scan(params, onDNSScan);
  }).click();

  $('.get-table-btn').click(function() {

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
      TableName: "SSLInformation"
    };
    docClient.scan(params, onScan);
  }).click();

  $('.get-pingdom-table-btn').click(function() {

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
      TableName: "PingdomInformation"
    };
    docClient.scan(params, onPingdomScan);
  }).click();

  function onScan(err, data) {
    if (err) {
      console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
      // print all the movies
      $(".ssl-table tbody").empty();
      data.Items.forEach(function(site) {
        var $row = $("<tr/>");
        if(site.siteStatus == "red") {
          $row.append("<td>" + site.name + "</td><td>" + site.url + "</td><td>" + site.expiration + "</td><td class=" + site.siteStatus + "><i class='fa fa-exclamation-square'></i></td><td>" + site.dateUpdated + "</td>");
        } else {
          $row.append("<td>" + site.name + "</td><td>" + site.url + "</td><td>" + site.expiration + "</td><td class=" + site.siteStatus + "><i class='fa fa-badge-check'></i></td><td>" + site.dateUpdated + "</td>");
        }

        $(".ssl-table tbody").append($row);
      });

      // continue scanning if we have more movies, because
      // scan can retrieve a maximum of 1MB of data
      if (typeof data.LastEvaluatedKey != "undefined") {
        console.log("Scanning for more...");
        params.ExclusiveStartKey = data.LastEvaluatedKey;
        docClient.scan(params, onScan);
      }
    }
  }
  function onWhoIsScan(err, data) {
    if (err) {
      console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
      // print all the movies
      $(".domain-table tbody").empty();
      data.Items.forEach(function(site) {
        var $row = $("<tr/>");
        if(site.siteStatus == "red") {
          $row.append("<td>" + site.url + "</td><td>" + site.name +  "</td><td>" + site.domainName + "</td><td>" + site.registrarServer + "</td><td>" + site.registrarURL
          + "</td><td>" + site.registrarName + "</td><td class=" + site.siteStatus + "><i class='fa fa-exclamation-square'></i></td><td>" + site.registrarExpiration + "</td>");
        } else {
          $row.append("<td>" + site.url + "</td><td>" + site.name +  "</td><td>" + site.domainName + "</td><td>" + site.registrarServer + "</td><td>" + site.registrarURL
          + "</td><td>" + site.registrarName + "</td><td class=" + site.siteStatus + "><i class='fa fa-badge-check'></i></td><td>" + site.registrarExpiration + "</td>");
        }
        $(".domain-table tbody").append($row);
      });

      // continue scanning if we have more movies, because
      // scan can retrieve a maximum of 1MB of data
      if (typeof data.LastEvaluatedKey != "undefined") {
        console.log("Scanning for more...");
        params.ExclusiveStartKey = data.LastEvaluatedKey;
        docClient.scan(params, onScan);
      }
    }
  }
  function onDNSScan(err, data) {
    if (err) {
      console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
      // print all the movies
      $(".dns-table tbody").empty();
      data.Items.forEach(function(site) {
        var $row = $("<tr/>");
        $row.append("<td>"+ site.name +"</td><td>" + site.aRecord + "</td><td>" + site.aaaaRecord + "</td><td>" + site.mxRecord + "</td><td>" + site.txtRecord + "</td><td>" + site.srvRecord
        + "</td><td>" + site.ptrRecord + "</td><td>"+ site.nsRecord + "</td><td>"+ site.cnameRecord + "</td>");

        $(".dns-table tbody").append($row);
      });

      // continue scanning if we have more movies, because
      // scan can retrieve a maximum of 1MB of data
      if (typeof data.LastEvaluatedKey != "undefined") {
        console.log("Scanning for more...");
        params.ExclusiveStartKey = data.LastEvaluatedKey;
        docClient.scan(params, onScan);
      }
    }
  }
  function onPingdomScan(err, data) {
    if (err) {
      console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
      // print all the movies
      $(".pingdom-table tbody").empty();
      data.Items.forEach(function(site) {
        var $row = $("<tr/>");
        if(site.siteStatus != "up") {
          $row.append("<td>" + site.id + "</td><td>" + site.dateCreated + "</td><td>" + site.siteName + "</td><td>" + site.hostname + "</td><td>" + site.siteType
          + "</td><td>" + site.lastTestTime + "</td><td class='red'><i class='fa fa-exclamation-square'></i></td><td>" + site.lastErrorTime + "</td>");
        } else {
          $row.append("<td>" + site.id + "</td><td>" + site.dateCreated + "</td><td>" + site.siteName + "</td><td>" + site.hostname + "</td><td>" + site.siteType
          + "</td><td>" + site.lastTestTime + "</td><td class='green'><i class='fa fa-badge-check'></i></td><td>" + site.lastErrorTime + "</td>");
        }
        $(".pingdom-table tbody").append($row);
      });

      // continue scanning if we have more movies, because
      // scan can retrieve a maximum of 1MB of data
      if (typeof data.LastEvaluatedKey != "undefined") {
        console.log("Scanning for more...");
        params.ExclusiveStartKey = data.LastEvaluatedKey;
        docClient.scan(params, onScan);
      }
    }
  }
  // whois buttons
  $('.update-domain').click(function() {
    whoisLambda.invoke(whoisPullParams, function(err, data) {
      if (err) {
        prompt(err);
      } else {
        pullResults = JSON.parse(data.Payload);
        displayWhoIsTable(pullResults);
      }
    });
  });
  // whois buttons
  $('.update-pingdom').click(function() {
    pingdomLambda.invoke(pingdomPullParams, function(err, data) {
      if (err) {
        prompt(err);
      } else {
        pullResults = JSON.parse(data.Payload);

        displayPingdomTable(pullResults);
      }
    });
  });
});

//you can now check that you can describe the DynamoDB table
// var params = {TableName: 'ssl-domain-check' };
// var dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'});
// dynamodb.describeTable(params, function(err, data){
//     console.log(JSON.stringify(data));
// }
